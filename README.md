usage

```
python3 pars3.py --list subdomains.txt --file uploadthisfile.txt --thread 50 -o output.txt

cat subdomains.txt | python3 pars3.py --stdin --file uploadthisfile.txt --thread 50 -o output.txt


python3 wordlist.py --domain twitter.com --wordlist namelist.txt | python3 pars3.py --stdin --file uploadthisfile.txt --thread 50 -o output.txt

python3 wordlist.py --list domains.txt --wordlist namelist.txt | python3 pars3.py --stdin --file uploadthisfile.txt --thread 50 -o output.txt
```
