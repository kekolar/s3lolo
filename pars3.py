import os
import sys
import boto3
import argparse
import threading
from colorama import *
from concurrent.futures import ThreadPoolExecutor

class scanner():

	def __init__(self):

		init(autoreset=True)
		self.print_lock = threading.Lock()
		self.subdomains = []

		if not args.file:

			print("You Need To Use --file param")

			sys.exit()

		else:

			if not os.path.exists(args.file):

				print("File Not Found >>",args.file)
				sys.exit()

		if args.list and not args.stdin:

			if not os.path.exists(args.list):
				print("Subdomain List Not Found")
				sys.exit()

			open_file = open(args.list, "r", encoding="utf-8").read().split("\n")

			self.subdomains.extend(list(set(filter(None, open_file))))

			del open_file

			if not len(self.subdomains) > 0:

				print("Subdomain List is empty")

				sys.exit()

		elif args.stdin and not args.list:

			open_stdin = sys.stdin.read().split("\n")

			self.subdomains.extend(list(set(filter(None, open_stdin))))

			del open_stdin

			if not len(self.subdomains) > 0:

				print("Not Found Subdomains From Stdin")

				sys.exit()

		else:

			print("Wrong Params")
			sys.exit()


		# dosya yüklemek için gerekli yer
		self.s3 = boto3.client('s3')

		# yüklenecek dosya
		self.file_path = args.file

		#yüklenecek dosya adı
		self.object_name = self.file_path


		#dosya silmek için gerekli yer
		self.s3_delete = boto3.resource('s3')


		with ThreadPoolExecutor(max_workers=args.thread) as executor:

			executor.map(self.start_attack, self.subdomains)

	def start_attack(self,subdomain):

		try:

			#dosya upload etme
			response = self.s3.upload_file(self.file_path, subdomain, self.object_name)

			with self.print_lock:
				print(Fore.MAGENTA+"[FILE UPLOADED]",str(subdomain))

			if args.output:
				self.save_output(f"[FILE UPLOADED] {subdomain}")

		except:
			pass

		try:

			#dosya silme
			self.s3_delete.Object(subdomain, self.file_path).delete()

			with self.print_lock:
				print(Fore.YELLOW+"[FILE REMOVED]",str(subdomain))

			if args.output:
				self.save_output(f"[FILE REMOVED] {subdomain}")
		except:
			pass


	def save_output(self,vulnerable):
	
		with open(args.output,"a+", encoding="utf-8") as f:

			f.write(str(vulnerable)+"\n")


if __name__ == "__main__":

	ap = argparse.ArgumentParser()
	ap.add_argument("-l", "--list", required=False, metavar="", help="Target List")
	ap.add_argument("-f", "--file", required=True, metavar="", help="File Path")
	ap.add_argument("-s", "--stdin", required=False, action="store_true", help="Read Subdomain on Stdin")
	ap.add_argument("-t", "--thread", required=False, default=20, type=int, metavar="", help="Thread Number(Default-20)")
	ap.add_argument("-o", "--output", required=False, metavar="", help="Save Ouput")
	args = ap.parse_args()

	start = scanner()
