import sys
import os
import argparse
import tldextract

def words():

        targets = []
        keys = []

        if args.domain and not args.list:
                targets.append(args.domain)

        elif args.list and not args.domain:
                if not os.path.exists(args.list):
                        print("Url List Not Found:",args.list)
                        sys.exit()

                open_file = open(args.list, "r", encoding="utf-8").read().split("\n")
                targets.extend(list(set(filter(None, open_file))))

                del open_file
                if not len(targets) > 0:
                        print("Your Wordlist Is Empty")
                        sys.exit()
        else:
                print("You Need To Use --domain Or --list params")
                sys.exit()

        if not os.path.exists(args.wordlist):
                print("Keyword List Not Found:",args.wordlist)

                sys.exit()

        open_file = open(args.wordlist, "r", encoding="utf-8").read().split("\n")
        keys.extend(list(set(filter(None, open_file))))


        if not len(keys) > 0:
                print("Your Keyword List Is Empty:",args.wordlist)
                sys.exit()

        del open_file

        for domains in targets:

                total_list = []

                parse = tldextract.extract(domains)
                domain_registered_name = parse.registered_domain
                domain_name = parse.domain
                domain_fqdn = parse.fqdn

                if len(parse.subdomain) > 0:
                        domain_sub = parse.subdomain

                        sub_dom = domain_sub + "." + domain_name

                for dicts in keys:

                        total_list.append(domain_registered_name + "." + dicts)
                        total_list.append(domain_registered_name + "-" + dicts)
                        total_list.append(domain_name + "." + dicts)
                        total_list.append(domain_name + "-" + dicts)
                        total_list.append(dicts + "." + domain_registered_name)
                        total_list.append(dicts + "-" + domain_registered_name)
                        total_list.append(dicts + "." + domain_name)
                        total_list.append(dicts + "-" + domain_name)
                        total_list.append(domain_name + dicts)
                        total_list.append(dicts + domain_name)



                        if len(parse.subdomain) > 0:

                                total_list.append(domain_fqdn + "." + dicts)
                                total_list.append(domain_fqdn + "-" + dicts)
                                total_list.append(dicts + "." + domain_fqdn)
                                total_list.append(dicts + "-" + domain_fqdn)
                                total_list.append(sub_dom + "." + dicts)
                                total_list.append(sub_dom + "-" + dicts)
                                total_list.append(dicts + "." + sub_dom)
                                total_list.append(dicts + "-" + sub_dom)


                total_list.sort()

                for run in total_list:

                        print(run)


                        if args.output:
                                save_output(run)

                del total_list



def save_output(out):

        with open(args.output, "a+") as file:
                file.write(str(out) + "\n")


if __name__ == "__main__":

        ap = argparse.ArgumentParser()
        ap.add_argument("-d", "--domain", metavar="", required=False, help="Single Target")
        ap.add_argument("-l", "--list", metavar="", required=False, help="Target List")
        ap.add_argument("-o", "--output", metavar="", required=False, help="Save Output")
        ap.add_argument("-w", "--wordlist", metavar="", required=True, help="Keyword List")

        args= ap.parse_args()
        start = words()
